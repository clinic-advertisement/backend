# Build docker binary
FROM golang:alpine
ADD . /go/src/clinic-advertisement-backend
WORKDIR /go/src/clinic-advertisement-backend
RUN go build -o main


# # Run application:
FROM alpine
COPY --from=0 /go/src/clinic-advertisement-backend go/src/clinic-advertisement-backend
WORKDIR /go/src/clinic-advertisement-backend/
CMD ./main -s

