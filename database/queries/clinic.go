package queries

import (

	// import modal

	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func CreateQueryFilterClinic(dbInstance *gorm.DB, c *gin.Context) *gorm.DB {

	// Chain filter method
	dbInstance = dbInstance.
		Table("clinics").
		Limit(10)

	// skip offset: lazy load
	offsetToSkip, errGetOffSetToSkip := strconv.Atoi(c.Query("next"))
	if errGetOffSetToSkip != nil {
		dbInstance = dbInstance.Where("clinics.id > ?", offsetToSkip)
	}

	if name := c.Query("name"); name != "" {
		dbInstance = dbInstance.Where("name like ?", "%"+name+"%")
	}

	if next := c.Query("next"); next != "" {
		dbInstance = dbInstance.Where("id > ?", next)
	}

	if uid := c.Query("uid"); uid != "" {
		dbInstance = dbInstance.Where("user_id = (?)", uid)
	}

	if districts := c.QueryArray("districts"); len(districts) != 0 {
		dbInstance = dbInstance.Where("district_id in (?)", districts)
	}

	if specialties := c.QueryArray("specialties"); len(specialties) != 0 {
		dbInstance = dbInstance.
			Joins("JOIN clinic_specialties on clinics.id = clinic_specialties.clinic_id").
			Where("clinic_specialties.specialty_id in (?)", specialties)
	}

	return dbInstance
}
