package seeds

type latlng struct {
	Lat float32
	Lng float32
}

type clinic struct {
	Name    string
	Address string
	Avatar  string
	Latlng  latlng
}

type districtClinic struct {
	Name    string
	Clinics []clinic
}

type SeedData struct {
	SpecialtyData      []string
	DistrictClinicData []districtClinic
}
