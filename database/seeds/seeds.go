package seeds

import (
	"clinic-advertisement-backend/database/models"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"path"
	"runtime"
	"time"

	"github.com/jinzhu/gorm"
)

func SeedDatabase(db *gorm.DB) {
	// resove path
	_, filename, _, _ := runtime.Caller(1)
	filepath := path.Join(path.Dir(filename), "./seeds/seeds.json")

	/*
		Import data from seed.json
		Use generic type: allow read any of stuff
	*/
	seedsFile, errOpenFile := os.Open(filepath)
	defer seedsFile.Close()
	if errOpenFile != nil {
		fmt.Errorf("Err when open file: %v", errOpenFile)
	}

	seedsDataBytes, errReadFile := ioutil.ReadAll(seedsFile)
	if errReadFile != nil {
		fmt.Errorf("Err when read file: %v", errReadFile)
	}

	var seedData SeedData
	json.Unmarshal([]byte(seedsDataBytes), &seedData)

	// Seed specialty
	seedSpecialty(seedData, db)

	// Seed 20 users
	seedUsers(20, db)

	// Seed notification of user
	seedNotification(20, db)

	// finally seed clinic
	seedDistrictAndClinicData(seedData, db)
}

func seedShift() {
	// User 1 will have 20 shift
}

func seedNotification(numOfUser int, db *gorm.DB) {
	for i := 1; i <= numOfUser; i++ {
		for j := 1; j <= 20; j++ {
			// Each users have 20 notification
			notification := models.Notification{
				IsRead:  false,
				UserID:  uint(i),
				Message: fmt.Sprintf("Dummy notification %v for user %v", j, i),
			}

			if err := db.Create(&notification).Error; err != nil {
				log.Fatal(err)
			}
		}
	}
}

func seedSpecialty(seedData SeedData, db *gorm.DB) {
	for _, specialty := range seedData.SpecialtyData {
		speciatyInstance := models.Specialty{Name: specialty}
		db.Create(&speciatyInstance)
	}
}

func seedUsers(num int, db *gorm.DB) {
	for i := 0; i < num; i++ {
		user := models.User{
			Avatar:   "https://www.imerir.com/wp-content/uploads/2017/02/ios7-contact_icon-icons.com_50286.png",
			Email:    fmt.Sprintf("user%v@nghia.com", i),
			Password: "123456",
		}

		db.Create(&user)
	}

	// Make the first user admin
	admin := models.Admin{UserID: 0}
	db.Create(&admin)
}

// http://golangcookbook.blogspot.com/2012/11/generate-random-number-in-given-range.html
func random(min, max int) int {
	rand.Seed(time.Now().Unix())
	return rand.Intn(max-min) + min
}

/*
	This function will increment numOfClinic
	If it reach miximum point = countSpecialty
	Reset it to 0
*/
func incrementNumOfSpecialty(numOfSpecialty *int, countSpecialty int) {
	// fmt.Println(numOfSpecialty)
	*numOfSpecialty++

	if *numOfSpecialty > countSpecialty {
		*numOfSpecialty = 1
	}
}

// must create at least 1 user to make this work
func seedDistrictAndClinicData(seedData SeedData, db *gorm.DB) {
	// count specialty
	countSpecialty := 0
	db.Model(models.Specialty{}).Count(&countSpecialty)

	// seed clinic specialty
	numOfClinic := 1

	// Use this for make sure that every specialty that have at least one clinic
	// user 1 have 20 first clinics, user 2 have other clinic
	numOfSpecialty := 1
	for _, districtClinicData := range seedData.DistrictClinicData {
		district := models.District{Name: districtClinicData.Name}

		// seed district
		db.Create(&district)

		// seed clinic of district
		for _, clinicData := range districtClinicData.Clinics {
			clinic := models.Clinic{
				Address:     clinicData.Address,
				Name:        clinicData.Name,
				Description: fmt.Sprintf("Dummy clinic %v", numOfClinic),
				Lat:         clinicData.Latlng.Lat,
				Lng:         clinicData.Latlng.Lng,
				DistrictID:  district.ID,
				// all clinic belong to user id 0
				UserID: 1,
			}

			db.Create(&clinic)

			// All clinic belong to 2 random specialty
			// Seed data of specialty belong to created clni
			clinicSpecialty1 := models.ClinicSpecialty{
				ClinicId:    clinic.ID,
				SpecialtyID: uint(numOfSpecialty),
			}

			db.Create(&clinicSpecialty1)
			incrementNumOfSpecialty(&numOfSpecialty, countSpecialty)

			clinicSpecialty2 := models.ClinicSpecialty{
				ClinicId:    clinic.ID,
				SpecialtyID: uint(numOfSpecialty),
			}

			db.Create(&clinicSpecialty2)
			incrementNumOfSpecialty(&numOfSpecialty, countSpecialty)

			numOfClinic++
		}
	}
}
