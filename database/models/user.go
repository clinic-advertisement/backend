package models

import "github.com/jinzhu/gorm"

/*
	User may own many clinics
*/
type User struct {
	gorm.Model
	Email    string `gorm:"unique_key"`
	Password string
	Avatar   string

	Clinics       []Clinic `gorm:"ForeignKey:UserID"`
	Shifts        []Shift
	Notifications []Notification
}
