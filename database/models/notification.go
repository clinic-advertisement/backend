package models

import "github.com/jinzhu/gorm"

type Notification struct {
	gorm.Model
	UserID  uint
	User    User `gorm:"PRELOAD:false"`
	Message string
	IsRead  bool
}
