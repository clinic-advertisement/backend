package models

// Composite foreign key
type ClinicSpecialty struct {
	ClinicId    uint `gorm:"primary_key"`
	SpecialtyID uint `gorm:"primary_key"`
}
