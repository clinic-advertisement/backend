package models

import (
	"github.com/jinzhu/gorm"
)

/*
	Clinic - belong 1 -> user
	Clinic - belong 1 -> districts
*/

type Clinic struct {
	gorm.Model

	User   User
	UserID uint `gorm:"reference user(id)"`

	District   District `gorm:"PRELOAD:false"`
	DistrictID uint

	Specialties []Specialty `gorm:"many2many:clinic_specialties;PRELOAD:false"`

	Shifts []Shift `gorm:"PRELOAD:false"`

	Name        string
	Address     string
	Lng         float32
	Lat         float32
	Description string
}
