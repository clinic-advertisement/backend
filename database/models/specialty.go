package models

import "github.com/jinzhu/gorm"

type Specialty struct {
	gorm.Model
	Name string `gorm:"unique_key"`

	Shifts  []Shift
	Clinics []Clinic `gorm:"many2many:clinic_specialties;"`
}
