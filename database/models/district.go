package models

import "github.com/jinzhu/gorm"

type District struct {
	gorm.Model
	Name string

	Clinics []Clinic `gorm:"PRELOAD:false"`
}
