package models

import "github.com/jinzhu/gorm"

type Shift struct {
	gorm.Model
	StartTime string `gorm:"type: timestamp"`
	EndTime   string `gorm:"type: timestamp"`

	ClinicID    uint
	UserID      uint
	SpecialtyID uint

	Specialty Specialty
	Clinic    Clinic `gorm:"PRELOAD:false"`
	User      User   `gorm:"PRELOAD:false"`
	Status    int    `gorm:"default: 0"`
}
