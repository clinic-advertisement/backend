package database

import (
	"clinic-advertisement-backend/database/models"
	"clinic-advertisement-backend/database/seeds"
	"log"
	"os"

	"fmt"

	// import modal

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func CreateDbInstance() (err error, db *gorm.DB) {
	conStr := configurateDbConnectionString()
	db, err = gorm.Open("postgres", conStr)

	if err != nil {
		log.Fatal(err)
	}

	return nil, db
}

func StartMigrate() {
	fmt.Println("Migrate in process...")

	// Get environment variable
	// env, isEnvExist := os.LookupEnv()
	// if isEnvExist == false
	// 	env = "local"

	// Extract from environment variable
	// env := "local"
	isCreateNew := os.Getenv("CREATE_NEW")
	isSeedDatabase := os.Getenv("SEED")

	// Create connection string and connect into database
	_, db := CreateDbInstance()
	defer db.Close()

	/* drop table type if create new: should drop in correct oder */
	if isCreateNew != "" {
		db.DropTableIfExists(&models.Shift{})
		db.DropTableIfExists(&models.ClinicSpecialty{})
		db.DropTableIfExists(&models.Clinic{})
		db.DropTableIfExists(&models.Specialty{})
		db.DropTableIfExists(&models.Admin{})
		db.DropTableIfExists(&models.Notification{})
		db.DropTableIfExists(&models.User{})
		db.DropTableIfExists(&models.District{})
	}

	// create table
	db.AutoMigrate(
		&models.User{},
		&models.Notification{},
		&models.Admin{},
		&models.Specialty{},
		&models.Clinic{},
		&models.ClinicSpecialty{},
		&models.Shift{},
		&models.District{},
	)

	if isSeedDatabase != "" {
		seeds.SeedDatabase(db)
	}

	// Create foreign key if create new
	if isCreateNew == "" {
		db.Model(models.Notification{}).AddForeignKey("user_id", "users(id)", "RESTRICT", "RESTRICT")
		db.Model(models.Admin{}).AddForeignKey("user_id", "users(id)", "RESTRICT", "RESTRICT")
		db.Model(models.Clinic{}).AddForeignKey("user_id", "users(id)", "RESTRICT", "RESTRICT")

		db.Model(models.Shift{}).AddForeignKey("user_id", "users(id)", "RESTRICT", "RESTRICT")
		db.Model(models.Shift{}).AddForeignKey("specialty_id", "specialties(id)", "RESTRICT", "RESTRICT")
		db.Model(models.Shift{}).AddForeignKey("clinic_id", "clinics", "RESTRICT", "RESTRICT")

		db.Model(models.ClinicSpecialty{}).AddForeignKey("clinic_id", "clinics(id)", "RESTRICT", "RESTRICT")
		db.Model(models.ClinicSpecialty{}).AddForeignKey("specialty_id", "specialties(id)", "RESTRICT", "RESTRICT")
	}

	defer fmt.Println("Migrate done !!!")
}

/*
	local and production environment may have different
	configurations
*/
func configurateDbConnectionString() string {
	/*
		if env is local then open five env.json and read database
		configurations key locals
	*/
	return os.Getenv("DATABASE_URL")
}
