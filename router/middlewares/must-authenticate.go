package middlewares

import (
	"clinic-advertisement-backend/utils"
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

func MustAuthenticateMiddleWare(c *gin.Context) {
	// validate header authorization
	authorizationHeader := c.GetHeader("Authorization")
	if authorizationHeader == "" {
		c.JSON(http.StatusUnauthorized, gin.H{
			"message": "No authorization header is presented",
		})
	}

	// validate token
	tokenString := strings.Split(authorizationHeader, "Bearer ")

	if tokenString[1] == "" {
		c.JSON(http.StatusUnauthorized, "")
	}
	uid, err := utils.VerifyJwt(tokenString[1])

	if err != nil {
		c.JSON(http.StatusUnauthorized, "")
	} else {
		c.Set("uid", fmt.Sprintf("%v", uid))
		c.Next()
	}
}
