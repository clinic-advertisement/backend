package router

import (
	"clinic-advertisement-backend/router/handlers/authenticate"
	"clinic-advertisement-backend/router/handlers/clinic"
	"clinic-advertisement-backend/router/handlers/notification"

	"github.com/gin-gonic/gin"
)

func RegisterRoutes(r *gin.Engine) {
	apiGroup := r.Group("/api")
	{

		clinicGroup := apiGroup.Group("/clinic")
		{
			clinicGroup.GET("/", clinic.GetClinicHandler)
			clinicGroup.POST("/", clinic.PostClinicHandler)
			clinicGroup.PUT("/:id", clinic.PutClinicHandler)
			clinicGroup.DELETE("/:id", clinic.DeleteClinicHandler)
		}

		notificationGroup := apiGroup.Group("notification")
		{
			notificationGroup.GET("/", notification.GetNotificationHandler)
			notificationGroup.PUT("/:id", notification.PutnotificationHandler)
		}

		r.POST("/login", authenticate.LoginHandler)
	}
}
