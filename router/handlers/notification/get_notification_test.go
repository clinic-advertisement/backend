package notification

import (
	"clinic-advertisement-backend/router/middlewares"
	"clinic-advertisement-backend/utils"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

/*
	3 cases
	get 5 last notification of current user
	get 5 next notification of current user
	return next = null if their is no notification left
*/

func createRouter() *gin.Engine {
	r := gin.Default()
	r.GET("/notification", middlewares.MustAuthenticateMiddleWare, GetNotificationHandler)
	return r
}

func TestGetNotification(t *testing.T) {
	/*
		STEP1: MAKE REQUEST
	*/
	router := createRouter()
	recorder := httptest.NewRecorder()
	request, _ := http.NewRequest("GET", "/notification", nil)
	utils.MakeAuthorizedHttpRequestOfUser1(request)

	router.ServeHTTP(recorder, request)

	// 200 with access_token
	assert.Equal(t, 200, recorder.Code)

	// since id1: first five notification will has id id: 1 -> 5 notifications. next = 5
	// all interface when unmarshal will have type float
	responseData := utils.ConvertJsonPostDataToMapStrInterface(recorder.Body)

	notifications := responseData["notifications"].([]interface{})

	// Check length and check id. notification[0]
	for i := 0; i < 5; i++ {
		notification := notifications[i].(map[string]interface{})
		uid := int(notification["ID"].(float64))
		if uid != i+1 {
			t.Errorf("notification id doesn't match. expect %v to %v", uid, i+1)
			return
		}
	}

	// Check next. then pass next to api bellow and then assert
	next := responseData["next"].(float64)
	assert.Equal(t, 5.0, next)
}

/*
	Each user when seed will 20 notifications
	GET NOTIFICATION id from 16->20 with next params = 15
*/
func TestGetNextNotification(t *testing.T) {
	/*
		STEP1: MAKE REQUEST
	*/
	router := createRouter()
	recorder := httptest.NewRecorder()
	request, _ := http.NewRequest("GET", "/notification?next=15", nil)
	utils.MakeAuthorizedHttpRequestOfUser1(request)

	router.ServeHTTP(recorder, request)

	// 200 with access_token
	assert.Equal(t, 200, recorder.Code)

	// since id1: first five notification will has id id: 1 -> 5 notifications. next = 5
	// all interface when unmarshal will have type float
	responseData := utils.ConvertJsonPostDataToMapStrInterface(recorder.Body)

	notifications := responseData["notifications"].([]interface{})

	// Check length and check id. notification[0]
	idToCompare := 16
	for i := 0; i < 5; i++ {
		notification := notifications[i].(map[string]interface{})
		uid := int(notification["ID"].(float64))
		if uid != idToCompare {
			t.Errorf("notification id doesn't match. expect %v to %v", uid, i+1)
			return
		}
		idToCompare++
	}

	// Check next.
	next := responseData["next"].(float64)
	assert.Equal(t, 20.0, next)
}

func TestGetOutOfRangeNotification(t *testing.T) {
	/*
		STEP1: MAKE REQUEST
	*/
	router := createRouter()
	recorder := httptest.NewRecorder()
	request, _ := http.NewRequest("GET", "/notification?next=20", nil)
	utils.MakeAuthorizedHttpRequestOfUser1(request)

	router.ServeHTTP(recorder, request)

	// 200 with access_token
	assert.Equal(t, 200, recorder.Code)

	// since id1: first five notification will has id id: 1 -> 5 notifications. next = 5
	// all interface when unmarshal will have type float
	responseData := utils.ConvertJsonPostDataToMapStrInterface(recorder.Body)

	notifications := responseData["notifications"].([]interface{})

	// Check length and check id. notification[0]
	if len(notifications) != 0 {
		t.Error("Should return zero notifications")
	} else if responseData["next"] != nil {
		t.Error("Should return next = nil")
	}
}
