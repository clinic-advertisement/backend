package notification

import (
	"clinic-advertisement-backend/database"
	"clinic-advertisement-backend/database/models"
	"clinic-advertisement-backend/utils"
	"errors"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

func PutnotificationHandler(c *gin.Context) {
	// get uid
	uid, uidExist := c.Get("uid")
	if !uidExist {
		utils.LogErrorWhenShitHappend(errors.New("Uid not error. Shit happend"), c)
	}

	// check if notification exist
	notificationID := c.Param("id")

	// open database connection
	errCreateDB, dbInstance := database.CreateDbInstance()
	if errCreateDB != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "error when connect data",
		})
	}

	// check if notification belong to user
	var notificationBelongToUser models.Notification
	dbInstance = dbInstance.
		Where("notifications.id=?", notificationID).
		Where("notifications.user_id=?", uid).
		First(&notificationBelongToUser)

	if dbInstance.RecordNotFound() {
		c.JSON(http.StatusNotFound, gin.H{})
		return
	} else if dbInstance.Error != nil {
		utils.LogErrorWhenShitHappend(dbInstance.Error, c)
		return
	}

	/*
		ID type uint and it's initial value is 0
		notification id from database start from id 1
	*/
	if notificationBelongToUser.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{})
	} else {
		notificationBelongToUser.IsRead = true

		// mark notification as read
		if err := dbInstance.
			Save(&notificationBelongToUser).Error; err != nil {
			log.Fatal(err)
			utils.LogErrorWhenShitHappend(err, c)
		} else {
			// return 200k
			c.JSON(http.StatusOK, gin.H{})
		}
	}
}
