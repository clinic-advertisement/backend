package notification

import (
	"clinic-advertisement-backend/database"
	"clinic-advertisement-backend/database/models"
	"clinic-advertisement-backend/utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

func GetNotificationHandler(c *gin.Context) {
	// get uid
	uid, _ := c.Get("uid")
	uidString := uid.(string)
	next := c.Query("next")

	// open connection
	err, dbInstance := database.CreateDbInstance()
	if err != nil {
		utils.LogErrorWhenShitHappend(err, c)
		return
	}

	// get notification belong to user
	var notifications []models.Notification
	dbInstance = dbInstance.
		Limit(5).
		Where("user_id = ?", uidString)

	// May chain more for search
	if next != "" {
		dbInstance = dbInstance.Where("id>?", next)
	}

	// start search stuff
	dbInstance = dbInstance.
		Find(&notifications)

	// return
	if dbInstance.Error != nil {
		utils.LogErrorWhenShitHappend(err, c)
	}

	var responseData = make(map[string]interface{})

	// If don't have data
	if dbInstance.RecordNotFound() != true {
		responseData["notifications"] = notifications
	}

	// next will be nil if no notification
	if len(notifications) == 0 {
		responseData["next"] = nil
	} else {
		responseData["next"] = notifications[len(notifications)-1].ID
	}

	c.JSON(http.StatusOK, responseData)
}
