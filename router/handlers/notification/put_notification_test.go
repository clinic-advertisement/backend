package notification

import (
	"clinic-advertisement-backend/database"
	"clinic-advertisement-backend/router/middlewares"
	"clinic-advertisement-backend/utils"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

/*
2 cases
put notification of current user - 200
put notification not belong to current user - 401
*/

func createPutNotificationRouter() *gin.Engine {
	r := gin.Default()
	r.PUT("/notification/:id", middlewares.MustAuthenticateMiddleWare, PutnotificationHandler)
	return r
}

func TestMarkAsReadNotificationBelongToUser(t *testing.T) {
	/*
		STEP1: MAKE REQUEST
	*/
	router := createPutNotificationRouter()
	recorder := httptest.NewRecorder()
	request, _ := http.NewRequest("PUT", "/notification/1", nil)
	utils.MakeAuthorizedHttpRequestOfUser1(request)

	router.ServeHTTP(recorder, request)

	// 200 with access_token
	assert.Equal(t, 200, recorder.Code)

	/*
		SETUP2: REASSEST DATABASE
	*/
	_, dbInstance := database.CreateDbInstance()
	if dbInstance.
		Table("notificatios").
		Where("id=?", 1).
		Where("is_read=?", 1).
		RecordNotFound() {
		t.Error("Not mark notification as unread")
	}
}

func TestMarkAsReadNotificationNotBelongToUser(t *testing.T) {
	/*
		STEP1: MAKE REQUEST
	*/
	router := createPutNotificationRouter()
	recorder := httptest.NewRecorder()

	// In seed data, each user will have 20 fake notification. sequel id start from 1
	request, _ := http.NewRequest("PUT", "/notification/21", nil)
	utils.MakeAuthorizedHttpRequestOfUser1(request)

	router.ServeHTTP(recorder, request)

	// 404
	assert.Equal(t, 404, recorder.Code)
}
