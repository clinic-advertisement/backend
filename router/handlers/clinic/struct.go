package clinic

type InputClinicStruct struct {
	Name        string `json:"name" binding:"required"`
	Specialties []uint `json:"specialties" binding:"required"`
	District    uint   `json:"district_id" binding:"required"`
	Address     string `json:"address" binding:"required"`

	// must be number
	Lng         float32 `json:"lat" binding:"required"`
	Lat         float32 `json:"lng" binding:"required"`
	Description string  `json:"description"`
}
