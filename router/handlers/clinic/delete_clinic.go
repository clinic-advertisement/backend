package clinic

import (
	"clinic-advertisement-backend/database"
	"clinic-advertisement-backend/database/models"
	"clinic-advertisement-backend/utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

func DeleteClinicHandler(c *gin.Context) {
	// open connection
	err, dbInstance := database.CreateDbInstance()
	if err != nil {
		utils.LogErrorWhenShitHappend(err, c)
		return
	}

	// If not found clinic, delete 404
	var clinicBeingDeleted models.Clinic
	errSearchClinc := dbInstance.
		Where("id=?", c.Param("id")).
		Find(&clinicBeingDeleted)

	if errSearchClinc.RecordNotFound() == true {
		c.JSON(http.StatusNotFound, gin.H{})
	}

	// Check if clinic belong to authenticated user; if not err 401
	uid, Ok := utils.GetUidFromContext(c)
	if !Ok {
		return
	}

	if clinicBeingDeleted.UserID != uid {
		// unauthorized
		c.JSON(http.StatusUnauthorized, gin.H{})
	}

	// Delete clinic; if fail err 500
	if err = dbInstance.Delete(clinicBeingDeleted).Error; err != nil {
		utils.LogErrorWhenShitHappend(err, c)
		return
	}

	// Delete ok, 200
	c.JSON(http.StatusOK, gin.H{})
}
