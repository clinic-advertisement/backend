package clinic

import (
	"clinic-advertisement-backend/database"
	"clinic-advertisement-backend/utils"
	"fmt"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/gin-gonic/gin"
)

/*
case 1: copy data one one clinic from database and query using that data
	next should be nil

case 2: test by lazy. Query by district. should return next data
	to query next

case 3: get next data bellow and query next

case 4: get clinic belong to many district

case 5: get clinic belong to many specialty
*/

/*
	recreate clinic data since lot's program touch it
*/
func TestSetup(t *testing.T) {
	// Remigrate data
	database.StartMigrate()
}

func setupGetClinicRouter() *gin.Engine {
	r := gin.Default()
	r.GET("/clinic", GetClinicHandler)
	return r
}

//
type ExtractIDStruct struct {
	ID string
}

func TestGetClinicAccuracy(t *testing.T) {
	/*
		dump data
		name: Bệnh Viện Huyện Bình Chánh
	*/
	r := setupGetClinicRouter()
	recorder := httptest.NewRecorder()

	// send alot of data via url so let's encode it, get clinic id1
	urlString := "/clinic"
	urlValues := make(url.Values)
	// name
	urlValues.Add("name", "Bệnh Viện")

	// district
	urlValues.Add("districts", "1")
	urlValues.Add("districts", " 2")

	// specialties
	urlValues.Add("specialties", "1")

	urlParamsEncode := urlValues.Encode()
	urlString = fmt.Sprintf("%v?%v", urlString, urlParamsEncode)

	// send url
	request := httptest.NewRequest("GET", urlString, nil)

	r.ServeHTTP(recorder, request)
	// assert data in the url: len = 1. id = 1, next should be 1. then pass next and agurment above to test bellow

	responseData := utils.ConvertJsonPostDataToMapStrInterface(recorder.Body)
	clinicsData := responseData["clinics"].([]interface{})

	if len(clinicsData) == 0 {
		t.Error("should return clinic with id 1")
	}

	clinics1 := clinicsData[0].(map[string]interface{})
	var id = clinics1["ID"].(float64)
	if id != 1.0 {
		t.Error("should return clinic with id 1")
	}

	// float 64
	next := responseData["next"].(float64)
	if next != 1.0 {
		t.Error("next should be one")
	}

}

func TestGetClinicLazyLoadNone(t *testing.T) {
	/*
		dump data
		name: Bệnh Viện Huyện Bình Chánh
	*/
	r := setupGetClinicRouter()
	recorder := httptest.NewRecorder()

	// send alot of data via url so let's encode it, get clinic id1
	urlString := "/clinic"
	urlValues := make(url.Values)
	// name
	urlValues.Add("name", "Bệnh Viện")

	// district
	urlValues.Add("districts", "1")
	urlValues.Add("districts", " 2")

	// specialties
	urlValues.Add("specialties", "1")
	urlValues.Add("next", "1")

	urlParamsEncode := urlValues.Encode()
	urlString = fmt.Sprintf("%v?%v", urlString, urlParamsEncode)

	// send url
	request := httptest.NewRequest("GET", urlString, nil)

	r.ServeHTTP(recorder, request)
	// assert data in the url: len = 1. id = 1, next should be 1. then pass next and agurment above to test bellow

	responseData := utils.ConvertJsonPostDataToMapStrInterface(recorder.Body)
	clinicsData := responseData["clinics"].([]interface{})

	if len(clinicsData) != 0 {
		t.Error("should return no clinic")
	}

	// float 64
	next := responseData["next"]
	if next != nil {
		t.Error("next should be one")
	}
}

func TestGetCinicLazyLoadNone(t *testing.T) {
	/*
		dump data
		name: Bệnh Viện Huyện Bình Chánh
	*/
	r := setupGetClinicRouter()
	recorder := httptest.NewRecorder()

	// send alot of data via url so let's encode it, get clinic id1
	urlString := "/clinic"
	urlValues := make(url.Values)
	// name
	urlValues.Add("next", "5")

	urlParamsEncode := urlValues.Encode()
	urlString = fmt.Sprintf("%v?%v", urlString, urlParamsEncode)

	// send url
	request := httptest.NewRequest("GET", urlString, nil)

	r.ServeHTTP(recorder, request)
	// assert data in the url: len = 1. id = 1, next should be 1. then pass next and agurment above to test bellow

	responseData := utils.ConvertJsonPostDataToMapStrInterface(recorder.Body)
	clinicsData := responseData["clinics"].([]interface{})

	if len(clinicsData) == 0 {
		t.Error("should return clinic")
	}

	// validate data. should return 10 clinic from 6->15
	idValidate := 6.0
	for i := 0; i < 10; i++ {
		clinc := clinicsData[i].(map[string]interface{})
		clinicID := clinc["ID"].(float64)
		if clinicID != idValidate {
			t.Errorf("Return wrong clinic. clinic id should be %v instead of %v", idValidate, clinicID)
		}
		idValidate++
	}

	// float 64
	next := responseData["next"].(float64)
	if next != 15.0 {
		t.Errorf("next should be 16 instead of %v", next)
	}
}
