package clinic

import (
	"clinic-advertisement-backend/database"
	"clinic-advertisement-backend/database/models"
	"clinic-advertisement-backend/router/middlewares"
	"clinic-advertisement-backend/utils"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/gin-gonic/gin"
)

func setupDeleteClinicRouter() *gin.Engine {
	r := gin.Default()
	r.DELETE("/clinic/:id", middlewares.MustAuthenticateMiddleWare, DeleteClinicHandler)
	return r
}

/*
	explain: all seed clinic data is belong to user id 1
*/
func TestDeleteClinicBelongToUser(t *testing.T) {
	r := setupDeleteClinicRouter()

	// create request with post clinic data
	recorder := httptest.NewRecorder()
	request, _ := http.NewRequest("DELETE", "/clinic/1", nil)

	utils.MakeAuthorizedHttpRequestOfUser(request, uint(1))

	r.ServeHTTP(recorder, request)

	// Create with post request
	assert.Equal(t, 200, recorder.Code)

	// Check db
	_, dbInstance := database.CreateDbInstance()
	var insertedClinic models.Clinic
	isNotFoundDeletedClinic := dbInstance.
		Where("id=?", 1).
		Find(&insertedClinic).
		RecordNotFound()

	assert.Equal(t, true, isNotFoundDeletedClinic)
}

func TestDeleteClinicNotBelongToUser(t *testing.T) {

	r := setupDeleteClinicRouter()

	// create request with post clinic data
	recorder := httptest.NewRecorder()
	request, _ := http.NewRequest("DELETE", "/clinic/2", nil)

	utils.MakeAuthorizedHttpRequestOfUser(request, uint(2))

	r.ServeHTTP(recorder, request)

	// Create with post request
	assert.Equal(t, 401, recorder.Code)
}
