package clinic

import (
	"clinic-advertisement-backend/database"
	"clinic-advertisement-backend/database/models"
	"clinic-advertisement-backend/utils"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func PutClinicHandler(c *gin.Context) {
	/*
		data required:
		name: str - required
		specialties: arr - required
		district: id
		lng: int - required
		lat: int - required
		userid: extract from jwt - required
		description - string
	*/

	err, dbInstance := database.CreateDbInstance()
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{
			"message": fmt.Sprintf("error when create db instance: %v", err),
		})
	}

	var clinicData InputClinicStruct
	if err = c.ShouldBindJSON(&clinicData); err != nil {
		// fmt.Print(err)
		utils.LogErrorWhenShitHappend(err, c)
		return
	}

	// map data: using decodefmt.Print(clinicData)

	// validate data

	// Get uid
	uid, ok := utils.GetUidFromContext(c)
	if !ok {
		return
	}

	// find clinic want to edit
	var editedClinic models.Clinic
	id := c.Param("id")
	fmt.Print(id)
	dbInstance.
		Where("id=?", c.Param("id")).
		Find(&editedClinic)

	// Check if editClinic exist
	if dbInstance.RecordNotFound() {
		c.AbortWithStatus(http.StatusNotFound)
	}

	// Check Check authenticate
	if editedClinic.UserID != uid {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	//

	editedClinic.Address = clinicData.Address
	editedClinic.Description = clinicData.Description
	editedClinic.DistrictID = clinicData.District
	editedClinic.Lng = clinicData.Lng
	editedClinic.Lat = clinicData.Lat
	editedClinic.Name = clinicData.Name

	// Assign clinics belongs to clinics
	var specialtiesAddToCreatedClinic []models.Specialty
	if dbInstance.
		Where("id in (?)", clinicData.Specialties).
		Find(&specialtiesAddToCreatedClinic).
		Error != nil {
		utils.LogErrorWhenShitHappend(err, c)
		return
	}
	editedClinic.Specialties = specialtiesAddToCreatedClinic

	errUpdatedClinic := dbInstance.Save(&editedClinic).Error
	if errUpdatedClinic != nil {
		c.AbortWithError(http.StatusAccepted, errUpdatedClinic)
	}

	// Create clinic success
	c.JSON(http.StatusOK, gin.H{})
}
