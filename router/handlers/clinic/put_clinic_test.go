package clinic

import (
	"clinic-advertisement-backend/database"
	"clinic-advertisement-backend/database/models"
	"clinic-advertisement-backend/router/middlewares"
	"clinic-advertisement-backend/utils"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func setupPutClinicRouter() *gin.Engine {
	r := gin.Default()
	r.PUT("/clinic/:id", middlewares.MustAuthenticateMiddleWare, PutClinicHandler)
	return r
}

func TestEditClinicNotBelongToUser(t *testing.T) {
	r := setupPutClinicRouter()

	// create request with post clinic data
	recorder := httptest.NewRecorder()
	postValue := map[string]interface{}{
		"description": "test la test",
		"lat":         1.123,
		"lng":         1.456,
		"district_id": 1,
		"name":        "clinic test belong user id 2",
		"address":     "test la test",
		"specialties": []int{1, 2, 3},
	}

	request, _ := http.NewRequest("PUT", "/clinic/5", utils.CreateJsonBodyData(postValue))
	request.Header.Set("Content-Type", "application/json")
	utils.MakeAuthorizedHttpRequestOfUser(request, uint(2))

	r.ServeHTTP(recorder, request)

	// Create with post request
	assert.Equal(t, 401, recorder.Code)
}

func TestEditClinicWithMalformData(t *testing.T) {
	r := setupPutClinicRouter()

	// create request with post clinic data
	recorder := httptest.NewRecorder()

	// mising lat attribute -> 400
	postValue := map[string]interface{}{
		"description": "test la test",
		"lng":         1.456,
		"district_id": 1,
		"name":        "clinic test belong user id 2",
		"address":     "test la test",
		"specialties": []int{1, 2, 3},
	}

	request, _ := http.NewRequest("PUT", "/clinic/5", utils.CreateJsonBodyData(postValue))
	request.Header.Set("Content-Type", "application/json")
	utils.MakeAuthorizedHttpRequestOfUser(request, uint(2))

	r.ServeHTTP(recorder, request)

	// Create with post request
	assert.Equal(t, 400, recorder.Code)
}

func TestEditClinicBelongToUser(t *testing.T) {
	r := setupPutClinicRouter()

	// create request with post clinic data
	recorder := httptest.NewRecorder()

	// mising lat attribute -> 400
	postValue := map[string]interface{}{
		"description": "test la test",
		"lng":         1.456,
		"lat":         1.456,
		"district_id": 1,
		"name":        "clinic test belong user id 1",
		"address":     "test la test",
		"specialties": []int{1, 2, 3},
	}

	request, _ := http.NewRequest("PUT", "/clinic/5", utils.CreateJsonBodyData(postValue))
	request.Header.Set("Content-Type", "application/json")
	utils.MakeAuthorizedHttpRequestOfUser(request, uint(1))

	r.ServeHTTP(recorder, request)

	// Create with post request
	assert.Equal(t, 200, recorder.Code)

	// Check db
	_, dbInstance := database.CreateDbInstance()
	var insertedClinic models.Clinic
	isNotFoundInsertedClinic := dbInstance.
		Where("name=?", "clinic test belong user id 1").
		Where("description=?", "test la test").
		Where("district_id=?", 1).
		Where("user_id=?", 1).
		Where("address=?", "test la test").
		Where("id=?", 5).
		Joins("JOIN clinic_specialties on clinics.id = clinic_specialties.clinic_id").
		Where("clinic_specialties.specialty_id in (?)", []int{1, 2, 3}).
		Find(&insertedClinic).
		RecordNotFound()

	assert.Equal(t, false, isNotFoundInsertedClinic)
}
