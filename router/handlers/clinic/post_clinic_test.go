package clinic

import (
	"clinic-advertisement-backend/database"
	"clinic-advertisement-backend/database/models"
	"clinic-advertisement-backend/router/middlewares"
	"clinic-advertisement-backend/utils"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/gin-gonic/gin"
)

func setupPostClinicRouter() *gin.Engine {
	r := gin.Default()
	r.POST("/clinic", middlewares.MustAuthenticateMiddleWare, PostClinicHandler)
	return r
}

func TestCreateClinicWithMalformData(t *testing.T) {
	r := setupPostClinicRouter()

	// create request with post clinic data
	recorder := httptest.NewRecorder()

	// mising lat attribute -> 400
	postValue := map[string]interface{}{
		"description": "test la test",
		"lng":         1.456,
		"district_id": 1,
		"name":        "clinic test belong user id 2",
		"address":     "test la test",
		"specialties": []int{1, 2, 3},
	}

	request, _ := http.NewRequest("POST", "/clinic", utils.CreateJsonBodyData(postValue))
	request.Header.Set("Content-Type", "application/json")
	utils.MakeAuthorizedHttpRequestOfUser(request, uint(2))

	r.ServeHTTP(recorder, request)

	// Create with post request
	assert.Equal(t, 400, recorder.Code)
}

/*
	Address:     clinicData.Address,
	Name:        clinicData.Name,
	Description: fmt.Sprintf("Dummy clinic %v", numOfClinic),
	Lat:         clinicData.Latlng.Lat,
	Lng:         clinicData.Latlng.Lng,
	DistrictID:  district.ID,
	// all clinic belong to user id 0
	UserID: 1,
*/
func TestCreateClinicWithEnoughData(t *testing.T) {
	r := setupPostClinicRouter()

	// create request with post clinic data
	recorder := httptest.NewRecorder()
	postValue := map[string]interface{}{
		"description": "test la test",
		"lat":         1.123,
		"lng":         1.456,
		"district_id": 1,
		"name":        "clinic test belong user id 2",
		"address":     "test la test",
		"specialties": []int{1, 2, 3},
	}

	request, _ := http.NewRequest("POST", "/clinic", utils.CreateJsonBodyData(postValue))
	request.Header.Set("Content-Type", "application/json")
	utils.MakeAuthorizedHttpRequestOfUser(request, uint(2))

	r.ServeHTTP(recorder, request)

	// Create with post request
	assert.Equal(t, 200, recorder.Code)

	// Check db
	_, dbInstance := database.CreateDbInstance()
	var insertedClinic models.Clinic
	isNotFoundInsertedClinic := dbInstance.
		Where("name=?", "clinic test belong user id 2").
		Where("description=?", "test la test").
		Where("district_id=?", 1).
		Where("user_id=?", 2).
		Where("address=?", "test la test").
		Joins("JOIN clinic_specialties on clinics.id = clinic_specialties.clinic_id").
		Where("clinic_specialties.specialty_id in (?)", []int{1, 2, 3}).
		Find(&insertedClinic).
		RecordNotFound()

	assert.Equal(t, false, isNotFoundInsertedClinic)

}
