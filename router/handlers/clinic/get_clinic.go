package clinic

import (
	"clinic-advertisement-backend/database"
	"clinic-advertisement-backend/database/models"
	"clinic-advertisement-backend/database/queries"
	"clinic-advertisement-backend/utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

type GetClinicStruct struct {
	name        string
	address     string
	lng         float32
	lat         float32
	description string
	id          uint
}

type GetClinicFilterParamsStruct struct {
	name         string
	districts    []string
	specialities []string
	user         string
	next         string
}

func GetClinicHandler(c *gin.Context) {
	/*
		Get 10 clinic:
		Search parameter
			name, string
			specialty:id, array
			district:id, array
			next=lazyload, id
			user=belong to user id
	*/

	// query
	errCreateDB, dbInstance := database.CreateDbInstance()
	if errCreateDB != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "error when connect data",
		})
	}

	// Chain filter method
	dbInstance = queries.CreateQueryFilterClinic(dbInstance, c)

	// start filter
	var clinics []models.Clinic
	dbInstance = dbInstance.
		Debug().
		Select("distinct(clinics.*)").
		Order("clinics.id").
		Scan(&clinics)

	// ignore record not found
	if dbInstance.RecordNotFound() != true {
		// error checking stuff
		if dbInstance.Error != nil {
			utils.LogErrorWhenShitHappend(dbInstance.Error, c)
			return
		}
	}

	// next should be id of lastest element. if clinics arr is empty then next = null
	responseData := make(map[string]interface{})
	responseData["clinics"] = clinics
	if len(clinics) == 0 {
		responseData["next"] = nil
	} else {

		responseData["next"] = clinics[len(clinics)-1].ID
	}

	// response with data
	c.JSON(http.StatusOK, responseData)
}
