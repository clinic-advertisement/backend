package clinic

import (
	"clinic-advertisement-backend/database"
	"clinic-advertisement-backend/database/models"
	"clinic-advertisement-backend/utils"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func PostClinicHandler(c *gin.Context) {
	/*
		data required:
		name: str - required
		specialties: arr - required
		district: id
		lng: int - required
		lat: int - required
		userid: extract from jwt - required
		description - string
	*/

	err, dbInstance := database.CreateDbInstance()
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{
			"message": fmt.Sprintf("error when create db instance: %v", err),
		})
	}

	var clinicData InputClinicStruct
	if err = c.ShouldBindJSON(&clinicData); err != nil {
		// fmt.Print(err)
		utils.LogErrorWhenShitHappend(err, c)
		return
	}

	// map data: using decodefmt.Print(clinicData)

	// validate data

	// Get uid
	uid, uidExist := c.Get("uid")

	// cast uint
	uidInt, _ := strconv.Atoi(uid.(string))

	if uidExist == false {
		utils.LogErrorWhenShitHappend(errors.New("no uid was present"), c)
		return
	}

	clinic := models.Clinic{
		Address:     clinicData.Address,
		Description: clinicData.Description,
		DistrictID:  clinicData.District,
		Lng:         clinicData.Lng,
		Lat:         clinicData.Lat,
		Name:        clinicData.Name,
		UserID:      uint(uidInt),
	}

	// Assign clinics belongs to clinics
	var specialtiesAddToCreatedClinic []models.Specialty
	if dbInstance.
		Where("id in (?)", clinicData.Specialties).
		Find(&specialtiesAddToCreatedClinic).
		Error != nil {
		utils.LogErrorWhenShitHappend(err, c)
		return
	}
	clinic.Specialties = specialtiesAddToCreatedClinic

	errCreateClinic := dbInstance.Create(&clinic).Error
	if errCreateClinic != nil {
		errCreateClinicStr := fmt.Sprintf("Error when create clinic %v", err)
		fmt.Print(errCreateClinicStr)
		c.JSON(http.StatusBadRequest, gin.H{
			"message": errCreateClinicStr,
		})
	}

	// Create clinic success
	c.JSON(http.StatusOK, gin.H{})
}
