package authenticate

import (
	"clinic-advertisement-backend/database"
	"clinic-advertisement-backend/database/models"
	"clinic-advertisement-backend/utils"
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

type LoginFormData struct {
	Email    string `form:"email" json:"email" xml:"email"  binding:"required"`
	Password string `form:"password" json:"password" xml:"password" binding:"required"`
}

/*
	Create database instance base on this settings
*/
func LoginHandler(c *gin.Context) {
	// Some super hot secret key
	/*
		Validate json chema
		username 	and email must not empty
	*/
	var loginFormData LoginFormData
	if errValidateSchema := c.ShouldBindJSON(&loginFormData); errValidateSchema != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": errValidateSchema.Error()})
		return
	}

	/*
		go to database and search for stuff
		If found then mine jwt
	*/
	_, dbInstance := database.CreateDbInstance()

	// findUserCondition := &models.User{Password: "", Email: ""}
	var foundUser models.User
	result := dbInstance.Where(&models.User{Email: loginFormData.Email, Password: loginFormData.Password}).First(&foundUser)
	if result.RecordNotFound() {
		// it not found 401
		c.JSON(http.StatusUnauthorized, gin.H{
			"message": "Wrong username or password",
		})
	} else {
		/*
			If found
			return mined jwt
		*/
		// Sign and get the complete encoded token as a string using the secret
		generatedTokenString, errSignJwt := utils.SignJwt(foundUser.ID)

		if errSignJwt != nil {
			log.Fatal(fmt.Sprintf("Can't sign verify token. errSignJwt: %v", errSignJwt))
			c.JSON(http.StatusBadRequest, gin.H{
				"message": fmt.Sprintf("Can't sign verify token. errSignJwt: %v", errSignJwt),
			})
		} else {
			c.JSON(http.StatusOK, gin.H{
				"access_token": generatedTokenString,
			})
		}
	}

	// ()

}
