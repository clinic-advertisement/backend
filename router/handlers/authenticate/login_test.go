package authenticate

import (
	"clinic-advertisement-backend/utils"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"

	"github.com/stretchr/testify/assert"
)

/*
case:
200: access_token when login success
401: when login not successfully
*/
func createRouter() *gin.Engine {
	r := gin.Default()
	r.POST("/login", LoginHandler)
	return r
}

func TestLoginHandlerPassed(t *testing.T) {
	router := createRouter()
	recorder := httptest.NewRecorder()

	postValue := map[string]interface{}{
		"email":    "user0@nghia.com",
		"password": "123456",
	}

	request, _ := http.NewRequest("POST", "/login", utils.CreateJsonBodyData(postValue))

	router.ServeHTTP(recorder, request)

	// 200 with access_token
	assert.Equal(t, 200, recorder.Code)
	// Check if it's return access token. Check if it has access token

	var responseUnmarshal = utils.ConvertJsonPostDataToMapStrInterface(recorder.Body)

	// check if it's has key
	if _, ok := responseUnmarshal["access_token"]; ok != true {
		t.Error("No access token was found in idtoken")
	}
}

func TestLoginHandlerFailure(t *testing.T) {
	router := createRouter()
	recorder := httptest.NewRecorder()

	postValue := map[string]interface{}{
		"email":    "user0@nghia.com",
		"password": "123456780000",
	}

	request, _ := http.NewRequest("POST", "/login", utils.CreateJsonBodyData(postValue))
	router.ServeHTTP(recorder, request)

	// 401: unauthorize
	assert.Equal(t, 401, recorder.Code)
}
