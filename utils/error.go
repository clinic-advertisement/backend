package utils

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func LogErrorWhenShitHappend(err error, c *gin.Context) {
	c.AbortWithError(http.StatusBadRequest, err)
}
