package utils

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func MakeAuthorizedHttpRequestOfUser(req *http.Request, id uint) {
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %v", CreateJwtOfUserId(id)))
}

func CreateJwtOfUserId(id uint) (tokenString string) {
	tokenString, _ = SignJwt(id)
	return
}

func GetUidFromContext(c *gin.Context) (uint, bool) {
	uid, uidExist := c.Get("uid")
	if uidExist != true {
		c.AbortWithError(http.StatusInternalServerError, errors.New("Can't not get uid"))
		return 0, true
	} else {
		// flow: str -> int -> uint... pretty sucks
		uidUint64, errConvertUidToInt := strconv.ParseUint(uid.(string), 10, 64)
		if errConvertUidToInt != nil {
			c.AbortWithError(http.StatusInternalServerError, errConvertUidToInt)
		}
		return uint(uidUint64), true

	}
}

func MakeAuthorizedHttpRequestOfUser1(req *http.Request) {
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %v", GetJwtOfUserId1()))
}

func GetJwtOfUserId1() (tokenString string) {
	tokenString, _ = SignJwt(1)
	return
}

func CreateJsonBodyData(postValue map[string]interface{}) io.Reader {
	postValueMarshal, _ := json.Marshal(postValue)
	return bytes.NewBuffer(postValueMarshal)
}

func ConvertJsonPostDataToMapStrInterface(jsonPostData *bytes.Buffer) map[string]interface{} {
	responseByte := jsonPostData.Bytes()
	var responseUnmarshal map[string]interface{}
	json.Unmarshal(responseByte, &responseUnmarshal)
	return responseUnmarshal
}
