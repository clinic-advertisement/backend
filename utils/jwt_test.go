package utils

import (
	"testing"
)

func TestSignAndVerifyJwtPassed(t *testing.T) {
	/*
		case 1 mine jwt and verify. expect to verify return propep
	*/
	goodJwtAccessToken, _ := SignJwt(0)
	uid, _ := VerifyJwt(goodJwtAccessToken)

	if uid != 0 {
		t.Errorf("uid should be %v instead of %v", 0, uid)
	}
}

func TestSignAndVerifyJwtFailed(t *testing.T) {
	/*
		case 2 pass bad random jwt to verify. expect to verify to return err
	*/
	goodJwtAccessToken := "somerandomstring"
	_, err := VerifyJwt(goodJwtAccessToken)

	if err == nil {
		t.Errorf("should throw error")
	}
}
