package utils

import (
	"fmt"
	"os"

	jwt "github.com/dgrijalva/jwt-go"
	jwtGo "github.com/dgrijalva/jwt-go"
)

func SignJwt(uid uint) (generatedTokenString string, err error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"uid": uid,
	})

	// Sign and get the complete encoded token as a string using the secret
	generatedTokenString, err = token.SignedString([]byte(os.Getenv("JWT_SECRETKEY")))

	return
}

type extractInterface struct {
	data int
}

func VerifyJwt(tokenString string) (uid uint, err error) {
	token, err := jwtGo.Parse(tokenString, func(token *jwtGo.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwtGo.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(os.Getenv("JWT_SECRETKEY")), nil
	})

	if err != nil {
		return
	}

	if claims, ok := token.Claims.(jwtGo.MapClaims); ok && token.Valid {
		uidInterface := claims["uid"]
		// uid, ok = uidInterface.(uint)
		// shit := uidInterface.(string)

		var uidFloat64 float64 = uidInterface.(float64)
		uid = uint(uidFloat64)

	}

	return
}
