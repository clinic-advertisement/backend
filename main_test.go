package main

import (
	"clinic-advertisement-backend/database"
	"testing"
)

func TestMain(t *testing.M) {
	database.StartMigrate()
}
