ifeq ($(DATABASE_URL),)
	DATABASE_URL:=host=localhost port=5432 user=postgres password=123456 dbname=clinic_advertisement_postgres sslmode=disable
endif

export DATABASE_URL
export JWT_SECRET="123456";

export POSTGRES_USER="postgres";
export POSTGRES_PASSWORD="123456";
export POSTGRES_DB="clinic_advertisement_postgres";

# Set as create database / data
export SEED="true";
export CREATE_NEW="true";
init:
	
db:
	docker run \
	--rm \
	-p 5432:5432 \
	--env "POSTGRES_USER=postgres" \
	--env "POSTGRES_PASSWORD=123456" \
	--env "POSTGRES_DB=clinic_advertisement_postgres" \
	--detach \
	--name clinic-advertisement-db \
		postgres:alpine

test:
	go	test	.;
	
	go	test	./utils;

	for directoryName in $$(ls ./router/handlers); do \
		go test "./router/handlers/$$directoryName"  ; \
	done
