package main

import (
	"clinic-advertisement-backend/database"
	"clinic-advertisement-backend/router"
	"os"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	// // Start migrations process
	database.StartMigrate()

	// // Register routes
	router.RegisterRoutes(r)

	// // start
	PORT := os.Getenv("PORT")
	if PORT == "" {
		PORT = "80"
	}

	r.Run(":" + PORT)

}
