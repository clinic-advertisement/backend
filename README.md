# Dependencies
* Gin: Favourite go framework specialize in microservice
* GORM: go orm

# Do this first
* glide install

# Usage
* Install docker
* Run ```make dev```
* Api avaialbe on localhost:3000

# Unit + Integration test
* make test

# Usage with docker (production mode)
* Build docker image
* Run with docker image

# Env variables

## Optional
* Create-new: set this if you want to drop all table
* Seed: set this if you want seed data

## Required
Postgres thing: remember to create database first
* POSTGRES_HOST
* POSTGRES_USER
* POSTGRES_PASSWORD
* POSTGRES_DB

# Start
* database/migrations/migrations.go -> startMigrate(): will start auto migration process when server start

* database/routes/routes.go -> registerRoutes(e echo_instance): will register all routes of our application. Then it will import all routes in routes directory and call methods RegisterRoute<Type>. eg: ```import

# Deploy
* use docker compose: ```docker-compose up``` will start server on http port 80

# Notice
* Edit seed data will make integration api test fail...

# Testing account
* username: user1@nghia.com
* password: 123456
